import React, { Component } from 'react';
import { DebounceInput } from 'react-debounce-input';
import LaTeX from 'latex-formula';
import PropTypes from 'prop-types';

class Input extends Component {
  render() {
    const { props } = this;
    return (
      <div className={props.className}>
        <label>
          <LaTeX inline formula={props.label + '='} />
        </label>
        <DebounceInput
          debounceTimeout={500}
          autoFocus={props.autoFocus}
          className={props.inputClassName}
          onChange={props.onChange}
          type="text"
          value={props.value}
        />
      </div>
    );
  }
}

Input.propTypes = {
  className: PropTypes.string,
  inputClassName: PropTypes.string,
  autoFocus: PropTypes.bool,
  onChange: PropTypes.func,
  value: PropTypes.string,
  label: PropTypes.string.isRequired,
}

Input.defaultProps = {
  value: '',
}

export default Input;
