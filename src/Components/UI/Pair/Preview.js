import React, { Component } from "react";
import nerdamer from "nerdamer";
import LaTeX from "latex-formula";

class Preview extends Component {
  expression() {
    if (this.props.value === '') {
      return {e: '', er: 'No Input'};
    }
    try {
      const e = nerdamer(this.props.value);
      return { e: e.toTeX() };
    } catch (er) {
      return { e: "", er: er.message };
    }
  }
  render() {
    const { props } = this;
    const ex = this.expression();
    return <LaTeX className={props.className} formula={ex.er ? ex.er : ex.e} />;
  };
}

export default Preview;
