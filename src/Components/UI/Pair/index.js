import React, {Component} from 'react';
import classes from './index.css';
import Input from './Input';
import Preview from './Preview';

export default class Pair extends Component {
  render() {
    return (
      <div className={classes.Pair}>
        <Input
          className={classes.InputWrap}
          inputClassName={classes.Input}
          autoFocus={!this.props.noAutoFocus}
          value={this.props.value}
          onChange={this.props.onChange}
          label='f(x)'
        />
        <Preview
          className={classes.Preview}
          value={this.props.value}
        />
      </div>
    )
  }
}
