import React from 'react';
import classes from './GenTask.css';
import Form from './Form';


export const genTask = props => {
  if (props.parentId) return props.back;
  return (
    <div>
      {props.setTask ? [
        <h3 key='1'>Создать задачу</h3>,
        <Form key='2' onSubmit={props.setTask} taskId={props.taskId} />,
        <h3 key='3'>Сгенерировать задачу</h3>
      ] : null}
      {props.levels.map((level, i) => (
        <button className={classes.Button} onClick={props.newTask(i)} key={level}>
          {level}
        </button>
      ))}
    </div>
  );
};

export default genTask;
