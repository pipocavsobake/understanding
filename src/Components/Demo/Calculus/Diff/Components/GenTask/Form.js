import React, { Component } from 'react';
import Pair from '~/Components/UI/Pair';
import PropTypes from 'prop-types';

class Form extends Component {
  state = {
    value: '',
  }
  render() {
    const { props } = this;
    return (
      <div>
        <Pair
          onChange={e => this.setState({value: e.target.value})}
          value={this.state.value}
        />
        <button onClick={() => props.onSubmit(this.state.value)} >Применить</button>
      </div>
    );
  }
}

Form.propTypes = {
  onSubmit: PropTypes.func,
}
export default Form;
