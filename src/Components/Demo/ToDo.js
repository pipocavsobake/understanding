import React from "react";
import LaTeX from "latex-formula";
import classes from "./ToDo.css";

const toDo = props => (
  <div className={classes.ToDo}>
    <h4>
      <span>{props.header}</span>
      {props.texPostfix ? <LaTeX inline formula={props.texPostfix} /> : null}
      {props.textPostfix ? <span>{props.textPostfix}</span> : null}
    </h4>
    <div className={classes.FormProizvSkroll}>
    {Array.isArray(props.tex) ? (
      <div>
        {props.tex.map((tex, key) => (
          <span className={classes.TeX} key={key}>
            <LaTeX inline formula={tex} />
          </span>
        ))}
      </div>
    ) : (
      <LaTeX formula={props.tex} />
    )}
    </div>
  </div>
);

export default toDo;
