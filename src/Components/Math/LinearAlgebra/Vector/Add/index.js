import React, { Component } from "react";
import LaTeX from "latex-formula";
import _ from "lodash";

// left, right, operator
export class Expression {
  constructor(left, operator, right) {
    this.left = left;
    this.operator = operator;
    this.right = right;
  }
  static tex(value) {
    return new Expression(value).tex();
  }
  texBinary() {
    const { left, right, operator } = this;
    return `{${Expression.tex(left)}}${operator}{${Expression.tex(right)}}`;
  }
  tex() {
    const { operator, left, right } = this;
    switch (operator) {
      case "+":
      case "-":
        return this.texBinary();
      case "->":
        return `\\overline{{${left}}{${right}}}`;
      default:
        return `left`;
    }
  }
  complexify(letters = 4) {
    //const ls = "ABCDEFFGH".slice(letters).split("")
    //const op = _.sample("+-".split(""))
    //if (this.operator === '->') {
    //const side = _.sample(["left", "right"])
    //const t = _.sample(ls.filter(l=>l!==this[side]))
    //if (side === "left") {
    //return new Expression(
    //new Expression(this.left, "->", t),
    //op,
    //op === '+' ? new Expression(t, "->", this.right) : new Expression(this.right)
    //}
    //}
  }
}

export const generate = () => {
  return new Expression("A", "->", "B");
};

class Add extends Component {
  state = {
    value: ""
  };

  change = e => {
    this.setState({ value: e.target.value });
  };

  render() {
    return (
      <div>
        <h2>Сократите выражение</h2>
        <LaTeX formula={this.props.expression} />
        <input onChange={this.change} value={this.state.value} />
      </div>
    );
  }
}

export default Add;
