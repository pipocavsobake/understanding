import React, { Component } from "react";
import PropTypes from "prop-types";

import LaTeX from "latex-formula";

export const generate = () => {};

export default class Proportion extends Component {
  static propTypes = {
    left: PropTypes.number.isRequired,
    right: PropTypes.number.isRequired,
    letters: PropTypes.arrayOf(PropTypes.string).isRequired
  };
  static defaultProps = {
    left: 1,
    right: 1,
    letters: ["A", "B", "C"]
  };
  state = {
    value: ""
  };

  change = e => {
    this.setState({ value: e.target.value });
  };

  task() {
    const [A, B, C] = this.props.letters;
    return (
      <span>
        <span>{`Напишите в векторном виде: «`}</span>
        <LaTeX inline formula={A} />
        <span> делит отрезок </span>
        <LaTeX inline formula={`${B}${C}`} />
        <span> в пропорции </span>
        <LaTeX inline formula={`${this.props.left}:${this.props.right}`} />
      </span>
    );
  }

  render() {
    return (
      <div>
        <h2>{this.task()}</h2>
        <div>
          <button
            style={{ textDecoration: "overline" }}
            onClick={this.overline}
          >
            A
          </button>
        </div>
        <div>
          <textarea
            rows="10"
            cols="40"
            onChange={this.change}
            value={this.state.value}
          />
        </div>
      </div>
    );
  }
}
