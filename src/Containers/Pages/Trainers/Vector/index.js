import React from "react";
import LinkItem from "~/Components/UI/LinkItem";

const VectorMenu = props => (
  <div>
    <LinkItem to="/trainers/vector-proportion" title="Деление отрезков" />
    <LinkItem to="/trainers/vector-add" title="Сложение векторов" />
  </div>
);

export default VectorMenu;
