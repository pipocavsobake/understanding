import Test from 'react-object-questions'
import {testStart} from '~/store/actions'
import {connect} from 'react-redux'

const mapStateToProps = ({test}) => ({test});
const mapDispatchToProps = dispatch => ({
  startTest: function(){
    dispatch(testStart(this.match.params.id))
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(Test);

