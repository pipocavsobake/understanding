import React, { Component } from "react";
import AddView, { generate } from "~/Components/Math/LinearAlgebra/Vector/Add";
import NewExample from "~/Components/UI/NewExample";
import { setCurrentError, removeCurrentError } from "~/store/actions";
import { connect } from "react-redux";
const mapDispatchToProps = dispatch => ({
  setError: er => dispatch(setCurrentError(er)),
  removeError: () => dispatch(removeCurrentError())
});
const AddViewConnected = connect(null, mapDispatchToProps)(AddView);

export default class VectorAdd extends Component {
  state = {
    v: ""
  };

  right = value => {
    console.log("Молодец", value);
  };

  gen = () => {
    this.setState({ v: generate().tex() });
  };
  render() {
    return (
      <div>
        <NewExample gen={this.gen} />
        <AddViewConnected expression={this.state.v} onRight={this.right} />
      </div>
    );
  }
}
