import React, { Component } from "react";
import ProportionView, {
  generate
} from "~/Components/Math/LinearAlgebra/Vector/Proportion";
import NewExample from "~/Components/UI/NewExample";
import { setCurrentError, removeCurrentError } from "~/store/actions";
import { connect } from "react-redux";
const mapDispatchToProps = dispatch => ({
  setError: er => dispatch(setCurrentError(er)),
  removeError: () => dispatch(removeCurrentError())
});
const ProportionViewConnected = connect(null, mapDispatchToProps)(
  ProportionView
);

export default class VectorAdd extends Component {
  state = {
    v: ""
  };

  right = value => {
    console.log("Молодец", value);
  };

  gen = () => {
    this.setState({ v: generate().tex() });
  };
  render() {
    return (
      <div>
        <NewExample gen={this.gen} />
        <ProportionViewConnected onRight={this.right} />
      </div>
    );
  }
}
